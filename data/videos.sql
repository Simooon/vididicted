SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `vididicted` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `vididicted`;

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `customer_id` int(11),
  `loanstate` boolean NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `customers`(
    `id` int(11) NOT NULL,
    `name` varchar(50) NOT NULL,
    `email` varchar(255) NOT NULL,
    `phone` varchar(20),
    `memberstate` ENUM('none','bronze','silver', 'gold') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `memberstate`) VALUES
(1, 'Seven', 'Seven@jax.com', '+41 69 420 69 69', 'none');

INSERT INTO `videos` (`id`, `title`, `loanstate`) VALUES
(1, 'Die Reise zum Mond', false),
(2, 'Der große Eisenbahnraub', false),
(3, 'Geburt einer Nation', false),
(4, 'Die Vampire', false),
(5, 'Intoleranz', false),
(6, 'Gebrochene Blüten', false),
(7, 'Das Cabinet des Dr. Caligari', false),
(8, 'Weit im Osten', false),
(9, 'Within Our Gates', false),
(11, 'Zwei Waisen im Sturm', false),
(12, 'Dr. Mabuse, der Spieler - Ein Bild der Zeit', false),
(13, 'Nosferatu, eine Symphonie des Grauens', false),
(14, 'Nanuk, der Eskimo', false),
(15, 'Das Lächeln der Madame Beudet', false),
(16, 'Die Hexe', false),
(17, 'Närrische Weiber', false),
(18, 'Die verflixte Gastfreundschaft', false),
(19, 'Das Rad', false),
(20, 'Der Dieb von Bagdad', false),
(21, 'Streik', false),
(22, 'Gier', false),
(23, 'Sherlock Jr.', false),
(24, 'The Great White Silence', false),
(25, 'Der letzte Mann', false),
(26, 'Buster Keaton, der Mann mit den 1000 Bräuten', false),
(27, 'Der Adler', false),
(28, 'Das Phantom der Oper', false),
(29, 'Panzerkreuzer Potemkin', false),
(30, 'Goldrausch', false),
(31, 'Die große Parade', false),
(32, 'Die Abenteuer des Prinzen Achmed', false),
(33, 'Metropolis', false),
(34, 'Sonnenaufgang - Lied von zwei Menschen', false),
(35, 'Der General', false),
(36, 'Der Unbekannte', false),
(37, 'Oktober', false),
(38, 'Der Jazzsänger', false),
(39, 'Napoleon', false),
(40, 'Der kleine Bruder', false),
(41, 'Ein Mensch der Masse', false),
(42, 'Die Docks von New York', false),
(43, 'Ein andalusischer Hund', false),
(44, 'Die Passion der Jungfrau von Orléans', false),
(45, 'Steamboat Bill, Jr. - Wasser hat keine Balken', false),
(46, 'Sturm über Asien', false),
(47, 'Erpressung', false),
(48, 'A Throw of Dice - Schicksalswürfel', false),
(49, 'Der Mann mit der Kamera', false),
(50, 'Die Büchse der Pandora', false),
(51, 'Der blaue Engel', false),
(52, 'Das goldene Zeitalter', false),
(53, 'Erde', false),
(54, 'Der kleine Cäsar', false),
(55, 'Im Westen nichts Neues', false),
(56, 'Es lebe die Freiheit', false),
(57, 'Die Million', false),
(58, 'Tabu', false),
(59, 'Dracula', false),
(60, 'Frankenstein', false),
(61, 'Lichter der Großstadt', false),
(62, 'Limit', false),
(63, 'Der öffentliche Feind', false),
(64, 'M - Eine Stadt sucht einen Mörder', false),
(65, 'Die Hündin', false),
(66, 'Vampyr - Der Traum des Allan Grey', false),
(67, 'Schönste, liebe mich', false),
(68, 'Boudu - aus den Wassern gerettet', false),
(69, 'Ich bin ein entflohener Kettensträfling', false),
(70, 'Ärger im Paradies', false),
(71, 'Narbengesicht', false),
(72, 'Shanghai Express', false),
(73, 'Freaks - Mißgestaltete', false),
(74, 'Me and My Gal', false),
(75, 'Betragen ungenügend', false),
(76, 'Die 42. Straße', false),
(77, 'Parade im Rampenlicht', false),
(78, 'Goldgräber von 1933', false),
(79, 'Sie tat ihm unrecht', false),
(80, 'Die Marx Brothers im Krieg', false),
(81, 'Königin Christine', false),
(82, 'Erde ohne Brot', false),
(83, 'King Kong und die weiße Frau', false),
(84, 'Das Verhängnis des General Yen', false),
(85, 'Die Wüstensöhne', false),
(86, 'Das ist geschenkt', false),
(87, 'Triumph des Willens', false),
(88, 'Atalante', false),
(89, 'Die schwarze Katze', false),
(90, 'Judge Priest', false),
(91, 'Es geschah in einer Nacht', false),
(92, 'Die Göttliche', false),
(93, 'Der dünne Mann', false),
(94, 'Peter Ibbetson', false),
(95, 'Unter Piratenflagge', false),
(96, 'Meuterei auf der Bounty', false),
(97, 'Die Marx Brothers in der Oper', false),
(98, 'Die 39 Stufen', false),
(99, 'Frankensteins Braut', false),
(100, 'Ich tanz mich in dein Herz hinein', false);

INSERT INTO `videos` VALUES (10, 'Der Fuhrmann des Todes',1 ,true);

ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

ALTER TABLE `customers`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `customers`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;