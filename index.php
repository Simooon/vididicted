<?php
require('model/database.php');
require('model/video_db.php');

$errorflag = false;

$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
$nameNew = filter_input(INPUT_POST, 'nameNew', FILTER_SANITIZE_STRING);
$emailNew = filter_input(INPUT_POST, 'emailNew', FILTER_VALIDATE_EMAIL);
$phoneNew = filter_input(INPUT_POST, 'phoneNew', FILTER_SANITIZE_STRING);
$memberstateNew = filter_input(INPUT_POST, 'memberstateNew', FILTER_SANITIZE_STRING);
$videoNew = filter_input(INPUT_POST, 'videoNew', FILTER_SANITIZE_STRING);

if($memberstateNew!='none'&&$memberstateNew!='bronze'&&$memberstateNew!='silver'&&$memberstateNew!='gold'&&$memberstateNew!=Null){
    $errorflag = true;
}

$videoSearch = filter_input(INPUT_GET, 'videoSearch', FILTER_SANITIZE_STRING);

$cusidUpdate = filter_input(INPUT_POST, 'customeridUpdate', FILTER_SANITIZE_STRING);
$vididUpdate = filter_input(INPUT_POST, 'videoidUpdate', FILTER_SANITIZE_STRING);
$videoUpdate = filter_input(INPUT_POST, 'videoUpdate', FILTER_SANITIZE_STRING);
$nameUpdate = filter_input(INPUT_POST, 'nameUpdate', FILTER_SANITIZE_STRING);
$emailUpdate = filter_input(INPUT_POST, 'emailUpdate', FILTER_VALIDATE_EMAIL);
$phoneUpdate = filter_input(INPUT_POST, 'phoneUpdate', FILTER_SANITIZE_STRING);
$loanstateUpdate = filter_input(INPUT_POST, 'loanstateUpdate', FILTER_SANITIZE_STRING);
$loanstateUpdate ? 1 : 0;
$memberstateUpdate = filter_input(INPUT_POST, 'memberstateUpdate', FILTER_SANITIZE_STRING);

if($memberstateUpdate!='none'&&$memberstateUpdate!='bronze'&&$memberstateUpdate!='silver'&&$memberstateUpdate!='gold'&&$memberstateUpdate!=Null){
    $errorflag = true;
}


$vididRemove = filter_input(INPUT_POST, 'videoidRemove', FILTER_SANITIZE_STRING);

$delId = filter_input(INPUT_POST, 'customeridDelete', FILTER_SANITIZE_STRING);

if (!$action) {
    $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
}


switch($action) {
    case 'insert':
        if ($nameNew && $emailNew && $memberstateNew && $videoNew &&!$errorflag) {
            $customerArray = customerExists($nameNew, $emailNew, $phoneNew);
            if (!$customerArray) {
                if(insertCustomer($nameNew, $emailNew, $phoneNew, $memberstateNew)){
                $customerArray = customerExists($nameNew, $emailNew, $phoneNew);
                bindVideoToCustomer($customerArray[0][0], $videoNew);
                    header("Location: .?added={$customerArray[0]}");
                } else{
                    $errorMessage = 'Error while trying to connect to the SQL-Database';
                    include('view/error.php');
                }
            } else {
                bindVideoToCustomer($customerArray[0][0], $videoNew);
                header("Location: .?added={$customerArray[0]}");
            }
        }else{
            $errorMessage = 'Invalid data. Check all fields and resubmit.';
            include('view/error.php');
        }
        break;
    case 'search':
        if ($videoSearch) {
            $result = searchVideo($videoSearch);
            include('view/update_delete_form.php');
        } else {
            $errorMessage = 'Invalid data. Check all fields and resubmit.';
            include('view/error.php');
        }
        break;
    case 'update':
        if ($cusidUpdate && $nameUpdate && $emailUpdate && $memberstateUpdate && $loanstateUpdate && $videoUpdate && $vididUpdate &&!$errorflag) {
            updateCustomer($cusidUpdate, $nameUpdate, $emailUpdate, $phoneUpdate, $memberstateUpdate, $loanstateUpdate, $videoUpdate, $vididUpdate);
            header("Location: .?updated={$nameUpdate}");
        } else {
            $errorMessage = 'Invalid data. Check all fields and resubmit.';
            include('view/error.php');
        }
        break;
    case 'remove':
        if ($vididRemove) {
            removeLoan($vididRemove);
            header("Location: .?removed={$vididRemove}");
        } else {
            $errorMessage = 'Invalid data. Check all fields and resubmit.';
            include('view/error.php');
        }
        break;
    case 'list':
        $result = listLoans();
        include("view/listview.php");
        break;
    case 'delete':
        if($delId){
            deleteCustomer($delId);
        }else{
            $errorMessage = '';
            include('view/error.php');
    }
    default:
        include('view/create_read_form.php');
}