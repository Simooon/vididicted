<?php
try {
    $db = new PDO('mysql:host=localhost;dbname=vididicted', "root");
} catch (PDOException $e) {
    $error_message = 'Database Error: ';
    $error_message .= $e->getMessage();
    include('view/error.php');
    exit();
}
