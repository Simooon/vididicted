<?php

function customerExists($name, $email, $phone=null){
    global $db;
    $query = "SELECT * FROM customers WHERE name LIKE '{$name}' AND email LIKE '{$email}' AND phone LIKE '{$phone}'";
    $statement = $db->prepare($query);
    if($statement->execute()){
        $result = $statement->fetchAll();
        $statement->closeCursor();
        return $result;
    }else{
        $statement->closeCursor();
        return false;
    }
}

function insertCustomer($name, $email, $phone = null, $memberstate){
    global $db;
    $query = "INSERT INTO customers (name, email, phone, memberstate) VALUES ('{$name}', '{$email}', '{$phone}', '{$memberstate}')";
    $statement = $db->prepare($query);

    if ($statement->execute()) {
        $statement->closeCursor();
        return true;
    } else{
        $statement->closeCursor();
        return false;
    };
}

function bindVideoToCustomer($id, $video){
    global $db;
    $query = "UPDATE videos SET customer_id = {$id}, loanstate = 1 WHERE title LIKE '{$video}'";
    $statement = $db->prepare($query);
    $statement->execute();
    $statement->closeCursor();
}

function updateCustomer($cusid, $name, $email, $phone, $memberstate, $loanstate, $video, $vidid){
    global $db;
    $query = "UPDATE customers SET name = '{$name}', email = '{$email}', phone = '{$phone}', memberstate = '{$memberstate}' WHERE id = {$cusid}";
    $statement = $db->prepare($query);
    $statement->execute();
    var_dump(customerExists($name, $email, $phone));
    $query = "UPDATE videos SET customer_id = {$cusid}, title ='{$video}', loanstate = {$loanstate} WHERE id = {$vidid}";
    $statement = $db->prepare($query);
    $statement->execute();
    var_dump(customerExists($name, $email, $phone));
    $statement->closeCursor();
}

function searchVideo($video){

    global $db;
    $query = "SELECT videos.title, customers.name, customers.email, customers.phone, videos.loanstate, customers.memberstate, customers.id, videos.id FROM videos JOIN customers ON customers.id = videos.customer_id WHERE videos.title LIKE '{$video}'";
    $statement = $db->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll();
    $statement->closeCursor();
    return $result ? $result[0] : array();
}

function removeLoan($vidid){

    global $db;
    $query = "UPDATE videos SET customer_id = NULL, loanstate = 0 WHERE id = {$vidid}";
    $statement = $db->prepare($query);
    $statement->execute();
    $statement->closeCursor();

}
function listLoans(){
    global $db;
    $query = "SELECT videos.id, videos.title, videos.loanstate, customers.id, customers.name, customers.email, customers.phone, customers.memberstate FROM videos JOIN customers ON customers.id = videos.customer_id ORDER BY videos.id";
    $statement = $db->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll();
    $statement->closeCursor();
    return $result;
}
function deleteCustomer($id){
    global $db;
    $query = "DELETE FROM customers WHERE id = {$id}";
    $statement = $db->prepare($query);
    $statement->execute();
    $query = "UPDATE videos SET customer_id = NULL, loanstate = 0 WHERE customer_id = {$id}";
    $statement = $db->prepare($query);
    $statement->execute();
    $statement->closeCursor();
}