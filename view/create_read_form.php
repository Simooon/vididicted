<?php include('header.html') ?>
    <section>
        <h2>Currently on Loan: </h2>
        <form action="." method="GET">
            <input type="hidden" name="action" value="search">
            <label>Video</label>
            <input type="text" name="videoSearch" required>
            <button>Search</button>
        </form>
    </section>
    <section>
        <h2>New Loan</h2>
        <form action="." method="POST">
            <input type="hidden" name="action" value="insert">
            <label>Name: </label>
            <input type="text" name="nameNew" required>
            <label>E-Mail:</label>
            <input type="text" name="emailNew" required>
            <label>Phone: </label>
            <input type="text" name="phoneNew">
            <label>Memberstate (none, bronze, silver, gold): </label>
            <input type="text"  name="memberstateNew" required>
            <label>Video: </label>
            <input type="text" name="videoNew" required>
            <button>Add</button>
        </form>
        <section>
        <form action="." method="POST">
            <input type="hidden", name="action", value="list">
            <button>List all Loans</button>
        </form>
        </section>
    </section>
    <section>
        <form action="." method="POST">
            <input type="hidden", name="action", value="delete">
            <label>Customer ID</label>
            <input type="text", name="customeridDelete">
            <input class="red" type="submit" onclick="return confirm('Are you Sure?')" value="Delete Customer">
        </form>
    </section>
<?php include('footer.html') ?>