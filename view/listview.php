<?php include("header.html") ?>
<section>
    <h2>Current Loans</h2>
    <table class="table">
        <tr>
            <th>Video ID</th>
            <th>Video Title</th>
            <th>Loanstate</th>
            <th>Customer ID</th>
            <th>Customer Name</th>
            <th>Customer Email</th>
            <th>Customer Phone</th>
            <th>Customer Memberstate</th>
        </tr>
        <?php foreach($result as $res):?>
                <tr>
                    <td><?=$res[0]?></td>
                    <td><?=$res[1]?></td>
                    <td><?=$res[2]?></td>
                    <td><?=$res[3]?></td>
                    <td><?=$res[4]?></td>
                    <td><?=$res[5]?></td>
                    <td><?=$res[6]?></td>
                    <td><?=$res[7]?></td>
                </tr>
        <?php endforeach; ?>
    </table>
</section>
<p><a href=".">Back to Request Forms</a></p>
<?php include('footer.html') ?>
