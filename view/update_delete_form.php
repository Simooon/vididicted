<?php include('header.html') ?>
<?php if ($result) {?>
    <section>
        <h2>Update Or Delete Data</h2>
            <form class="update" action="." method="POST">
                <input type="hidden" name="action" value="update">
                <label>Video:</label>
                <input type="text" name="videoUpdate" value="<?= $result[0] ?>" readonly>
                <label>Customer name:</label>
                <input type="text" name="nameUpdate" value="<?= $result[1] ?>" required>
                <label>Email:</label>
                <input type="text" name="emailUpdate" value="<?= $result[2] ?>" required>
                <label>Phonenumber:</label>
                <input type="text" name="phoneUpdate" value="<?= $result[3] ?>" required>
                <label>Is on Loan:</label>
                <input type="text" name="loanstateUpdate" value="<?= $result[4] ? "true" : "false" ?>" required readonly>
                <label>Memberstate</label>
                <input type="text" name="memberstateUpdate" value="<?= $result[5] ?>" required>
                <input type="hidden", name="customeridUpdate", value="<?= $result[6]?>">
                <input type="hidden", name="videoidUpdate", value="<?= $result[7]?>">
                <button>Update</button>
            </form>
        <form action="." method="POST">
            <input type="hidden", name="action", value="remove">
            <input type="hidden", name="videoidRemove", value="<?=$result[7]?>">
            <button class="red">Delete</button>
        </form>
    </section>
<?php } else { ?>
    <p>The searched Video is currently not on Loan.</p>
<?php } ?>
    <p><a href=".">Back to Request Forms</a></p>
<?php include('footer.html') ?>